/* (c) https://github.com/MontiCore/monticore */
#include "experiment_manager_types.hpp"

namespace experiment_manager {
    Checkpoint::Checkpoint(bool higher, bool useX, double value) : higher(higher), useX(useX),
                                                                   value(value) {}

    bool Checkpoint::isAfter(geometry_msgs::PoseWithCovariance poseWithCovariance) {
        auto position = poseWithCovariance.pose.position;
        double compVal = this->useX ? position.x : position.y;
        if (this->higher) {
            return compVal > this->value;
        } else {
            return compVal < this->value;
        }

    }
}
