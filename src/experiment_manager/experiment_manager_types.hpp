/* (c) https://github.com/MontiCore/monticore */
#pragma once

#include <geometry_msgs/PoseWithCovariance.h>
#include <geometry_msgs/Point.h>
#include <ostream>


namespace experiment_manager {
    class Checkpoint {
    public:
        Checkpoint(bool higher, bool useX, double value);

        Checkpoint() {}

        bool isAfter(geometry_msgs::PoseWithCovariance);

        bool higher;
        bool useX;
        double value;
    };


    class CarConfig {
    public:
        CarConfig(const geometry_msgs::Point &posOffset, double velocity) : startPos(posOffset), velocity(velocity) {}

        geometry_msgs::Point startPos;
        double velocity;

        friend std::ostream &operator<<(std::ostream &os, const CarConfig &config) {
            os << "startPos: " << config.startPos << " velocity: " << config.velocity;
            return os;
        }
    };

    class ExperimentResult {
    public:
        double timeToFinish = 0;
        bool crash = false;

        friend std::ostream &operator<<(std::ostream &os, const ExperimentResult &result) {
            os << "timeToFinish: " << result.timeToFinish << " crash: " << result.crash;
            return os;
        }
    };

    class ExperimentConfig {
    public:
        ExperimentConfig() {}

        explicit ExperimentConfig(int id) : id(id) {
        }

        int id;

        std::vector<experiment_manager::CarConfig> carConfigurations;
    };

    class EnvConfig {
    public:
        EnvConfig() {}

        double delay;
        double dropChance;
        bool interConActive;
        double timeCutoff;
        double holdTime;


        EnvConfig(double delay, double dropChance, bool interConActive, double timeCutoff, double holdTime) : delay(delay),
                                                                                                              dropChance(dropChance),
                                                                                                              interConActive(interConActive),
                                                                                                              timeCutoff(timeCutoff),
                                                                                                              holdTime(holdTime) {}

        friend std::ostream &operator<<(std::ostream &os, const EnvConfig &config) {
            os << "delay: " << config.delay << " dropChance: " << config.dropChance << " interConActive: "
               << config.interConActive << " timeCutoff: " << config.timeCutoff << " holdTime: " << config.holdTime;
            return os;
        }
    };
}
