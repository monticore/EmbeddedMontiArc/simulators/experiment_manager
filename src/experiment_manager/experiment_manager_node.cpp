/* (c) https://github.com/MontiCore/monticore */
#include "experiment_manager.hpp"

int main(int argc, char* argv[]) {

    ros::init(argc, argv, "experiment_manager_node");

    experiment_manager::Experiment_manager experiment_manager(ros::NodeHandle(), ros::NodeHandle("~"));

    ros::spin();
    return 0;
}
