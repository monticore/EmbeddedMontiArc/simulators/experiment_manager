/* (c) https://github.com/MontiCore/monticore */
#pragma once
#include "experiment_manager_types.hpp"
#include <ros/ros.h>
#include <string>
#include <map>
#include <automated_driving_msgs/ObjectStateArray.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/String.h>
#include <regex>
#include <fstream>
#include <dynamic_reconfigure/DoubleParameter.h>
#include <dynamic_reconfigure/Reconfigure.h>
#include <dynamic_reconfigure/Config.h>
#include "experiment_manager/Experiment_managerParameters.h"

namespace experiment_manager {

    class Experiment_manager {
    public:
        Experiment_manager(ros::NodeHandle, ros::NodeHandle);

        void startNextExperiment();

    private:
        Experiment_managerParameters params_;

        ros::Time lastStart;
        bool resetVelPublished = false;
        bool isInitiated = false;
        bool finished = false;
        bool lastPublish = true;
        uint n = 2;
        uint curExpIndex = 0;
        uint curEnvIndex = 0;
        uint lastExpIndex = 0;
        uint lastEnvIndex = 0;
        ExperimentResult curRes;
        std::map<uint, Checkpoint> idToCheckpoint;
        std::map<uint, double> idToEndtime;
        std::map<uint, bool> behindCheckpoint;
        std::vector<experiment_manager::ExperimentConfig> expConfigs;

        std::vector<experiment_manager::EnvConfig> envConfigs;

        automated_driving_msgs::ObjectStateArray lastOSA;
        //Sub and Pub
        ros::Subscriber groundTruthSub;
        ros::Subscriber collisionSub;
        ros::Publisher clearCommQueuePub;
        ros::Publisher resetObjectPosePub;
        ros::Publisher controllerActivePub;
        ros::Publisher resultPub;
        ros::Publisher timeCutoffPub;
        std::vector<ros::Publisher> holdTimePubs;
        std::vector<ros::Publisher> maxVelPubs;
        std::vector<ros::Publisher> maxAccelPubs;
        std::vector<ros::Publisher> resetVelPubs;
        std::vector<ros::Publisher> trajectoryPubs;

        void groundTruthCallback(const automated_driving_msgs::ObjectStateArray::ConstPtr &msg);

        void collisionCallback(const std_msgs::Bool::ConstPtr &msg);

        void loadExperimentConfigs();

        bool handleCheckpoints(const automated_driving_msgs::ObjectStateArray::ConstPtr &msg);

        void publishInitValues();

        void resetResult();

        void loadEnvironmentConfigs();

        void publishEnvironmentConfig(EnvConfig &envConfig);

        void setReconfigureParam(std::string param, double value);

        double readNextAsDouble(std::stringstream &stream);

        int readNextAsInt(std::stringstream &stream);

        bool readNextAsBool(std::stringstream &stream);

        void publishResult();

        void publishDouble(ros::Publisher &pub, double value);

        void publishBool(ros::Publisher &pub, bool value);

        void nextIndices();

        void publishExperimentConfig(const ExperimentConfig &curExp);

        void publishString(ros::Publisher &pub, std::string &value);

        void resultToFile(std::string &resultString);

        void publishInitTrajectory(ros::Publisher &publisher, unsigned int id);
    };
}
