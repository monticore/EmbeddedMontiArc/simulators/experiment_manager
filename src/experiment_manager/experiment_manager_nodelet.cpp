/* (c) https://github.com/MontiCore/monticore */
#include "experiment_manager.hpp"
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>

namespace experiment_manager {

class Experiment_managerNodelet : public nodelet::Nodelet {

    virtual void onInit();
    boost::shared_ptr<Experiment_manager> m_;
};

void Experiment_managerNodelet::onInit() {
    m_.reset(new Experiment_manager(getNodeHandle(), getPrivateNodeHandle()));
}

}

PLUGINLIB_DECLARE_CLASS(experiment_manager,
                        Experiment_managerNodelet,
                        experiment_manager::Experiment_managerNodelet,
                        nodelet::Nodelet);
