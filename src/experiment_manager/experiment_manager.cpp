/* (c) https://github.com/MontiCore/monticore */
#include "experiment_manager.hpp"

namespace experiment_manager {

    Experiment_manager::Experiment_manager(ros::NodeHandle node_handle, ros::NodeHandle private_node_handle) : params_{
            private_node_handle} {
        params_.fromParamServer();

        loadExperimentConfigs();
        loadEnvironmentConfigs();

        idToCheckpoint.insert(std::make_pair<uint, Checkpoint>(1u, Checkpoint(true, false, 5430177.22946)));
        idToCheckpoint.insert(std::make_pair<uint, Checkpoint>(2u, Checkpoint(true, true, 458481.243194)));

        /**
         * Publishers & subscriber
         */
        resultPub = node_handle.advertise<std_msgs::String>("/experiment_manager/results", params_.msg_queue_size);

        groundTruthSub = node_handle.subscribe(params_.ground_truth_topic, params_.msg_queue_size,
                                               &Experiment_manager::groundTruthCallback, this,
                                               ros::TransportHints().tcpNoDelay());

        collisionSub = node_handle.subscribe(params_.collision_topic, params_.msg_queue_size,
                                             &Experiment_manager::collisionCallback, this,
                                             ros::TransportHints().tcpNoDelay());

        resetObjectPosePub = node_handle.advertise<automated_driving_msgs::ObjectState>(params_.reset_object_pose_topic,
                                                                                        params_.msg_queue_size);

        controllerActivePub = node_handle.advertise<std_msgs::Bool>(params_.controller_active_topic,
                                                                    params_.msg_queue_size);

        timeCutoffPub = node_handle.advertise<std_msgs::Float64>(params_.cutoff_time_topic, params_.msg_queue_size);
        clearCommQueuePub = node_handle.advertise<std_msgs::Bool>(params_.clear_queue_topic, params_.msg_queue_size);

        std::regex indexRegex("\\{i\\}");

        for (uint i = 1; i <= n; i++) {
            const std::string &indexString = std::to_string(i);
            std::string maxVelTopic = std::regex_replace(params_.max_vel_topic_template, indexRegex, indexString);
            std::string maxAccelTopic = std::regex_replace(params_.max_accel_topic_template, indexRegex, indexString);
            std::string resetVelTopic = std::regex_replace(params_.reset_vel_topic_template, indexRegex, indexString);
            std::string holdTimeTopic = std::regex_replace(params_.hold_time_topic_template, indexRegex, indexString);
            std::string trajectoryTopic = std::regex_replace(params_.trajectory_topic_template, indexRegex, indexString);
            maxVelPubs.push_back(node_handle.advertise<std_msgs::Float64>(maxVelTopic, params_.msg_queue_size));
            maxAccelPubs.push_back(node_handle.advertise<std_msgs::Float64>(maxAccelTopic, params_.msg_queue_size));
            resetVelPubs.push_back(node_handle.advertise<std_msgs::Bool>(resetVelTopic, params_.msg_queue_size));
            holdTimePubs.push_back(node_handle.advertise<std_msgs::Float64>(holdTimeTopic, params_.msg_queue_size));
            trajectoryPubs.push_back(node_handle.advertise<std_msgs::Float64MultiArray>(trajectoryTopic, params_.msg_queue_size));
        }
    }

    void Experiment_manager::groundTruthCallback(const automated_driving_msgs::ObjectStateArray::ConstPtr &msg) {
        this->lastOSA = *msg;

        //make sure the velocity controllers have enough time to reset(0.25 sec after experiment start)
        double now = ros::Time::now().toSec();
        if (isInitiated && now - lastStart.toSec() > 0.25) {
            if (!resetVelPublished) {
                for (uint i = 0; i < n; i++) {
                    publishBool(resetVelPubs[i], false);
                    resetVelPublished = true;
                }
            }
        } else {
            resetVelPublished = false;
        }

        bool allBehindCheckpoint = handleCheckpoints(msg);

        for (uint i = 0; i < n; i++) {
            double curMaxVel = expConfigs[curExpIndex].carConfigurations[i].velocity;
            publishDouble(maxVelPubs[i], behindCheckpoint[i + 1] ? 0 : curMaxVel);
        }

        publishInitValues();

        if (allBehindCheckpoint) {
            if (isInitiated) {
                double timeToFinish = now - lastStart.toSec();
                curRes.timeToFinish = timeToFinish;
            }
            //Wait till cars are reset
            if (now - lastStart.toSec() > 1) {
                if (!finished) {
                    startNextExperiment();
                }else if(lastPublish){
                    publishResult();
                    lastPublish = false;
                    ROS_INFO("Finished all experiments!");
                }
            }
        }
    }

    void Experiment_manager::collisionCallback(const std_msgs::Bool::ConstPtr &msg) {
        if (msg->data) {
            ROS_WARN_THROTTLE(1, "Collision detected!");
            curRes.crash = true;
        }
    }

    bool Experiment_manager::handleCheckpoints(const automated_driving_msgs::ObjectStateArray::ConstPtr &msg) {
        bool allBehindCheckpoint = true;
        for (auto obj : msg->objects) {
            Checkpoint &checkpoint = idToCheckpoint[obj.object_id];
            if (checkpoint.isAfter(obj.motion_state.pose)) {
                //save first time in endtime
                if (idToEndtime.count(obj.object_id) == 0) {
                    idToEndtime[obj.object_id] = ros::Time::now().toSec();
                }
                behindCheckpoint[obj.object_id] = true;
            } else {
                behindCheckpoint[obj.object_id] = false;
                allBehindCheckpoint = false;
            }
        }
        return allBehindCheckpoint;
    }

    void Experiment_manager::publishInitValues() {
        if (!isInitiated) {
            for (uint i = 0; i < n; i++) {
//                ROS_INFO("In init: Publish maxVel[%u] = %f", i, maxVelMsg.data);
                publishDouble(maxVelPubs[i], 2);
                publishDouble(maxAccelPubs[i], 0.2 * 9.81);
                publishBool(resetVelPubs[i], true);
            }
        }
    }

    void Experiment_manager::publishDouble(ros::Publisher &pub, double value) {
        std_msgs::Float64 tmpMsg;
        tmpMsg.data = value;
        pub.publish(tmpMsg);
    }

    void Experiment_manager::publishBool(ros::Publisher &pub, bool value) {
        std_msgs::Bool tmpMsg;
        tmpMsg.data = value;
        pub.publish(tmpMsg);
    }

    void Experiment_manager::publishString(ros::Publisher &pub, std::string &value) {
        std_msgs::String tmpMsg;
        tmpMsg.data = value;
        pub.publish(tmpMsg);
    }

    void Experiment_manager::setReconfigureParam(std::string param, double value) {
        dynamic_reconfigure::Config conf;
        dynamic_reconfigure::DoubleParameter doubleParameter;
        dynamic_reconfigure::ReconfigureRequest reconfigureRequest;
        dynamic_reconfigure::ReconfigureResponse reconfigureResponse;

        doubleParameter.value = value;
        doubleParameter.name = param;
        conf.doubles.push_back(doubleParameter);

        reconfigureRequest.config = conf;




        for (uint i = 1; i <= n; i++) {
            const std::string &indexString = std::to_string(i);
            ros::service::call("/comm_mgmt_converter_"+ indexString +"/set_parameters", reconfigureRequest,
                           reconfigureResponse);
            ros::service::call("/v"+indexString+"/comm/com_mgmt_slowDown"+indexString+"/set_parameters",
                               reconfigureRequest, reconfigureResponse);
        }
    }

    void Experiment_manager::startNextExperiment() {
        lastStart = ros::Time::now();
        //only publish
        if (isInitiated) {
            publishResult();
        } else {
            isInitiated = true;
        }
        resetResult();
        ROS_INFO("[%lu/%lu]Starting experiment: envIndex, expIndex = %u, %u",curEnvIndex * expConfigs.size() + curExpIndex + 1,envConfigs.size() * expConfigs.size(), curEnvIndex, curExpIndex);
        publishEnvironmentConfig(envConfigs[curEnvIndex]);
        publishExperimentConfig(expConfigs[curExpIndex]);
        nextIndices();
    }

    void Experiment_manager::publishExperimentConfig(const ExperimentConfig &curExp) {
        if (curExp.carConfigurations.size() != lastOSA.objects.size()) {
            ROS_ERROR("The number of vehicles and the specified experiment dont match! Experiment:%lu, objects:%lu",
                      curExp.carConfigurations.size(), lastOSA.objects.size());
        }

        for (auto &obj :lastOSA.objects) {
            geometry_msgs::Point tmpPosition;
            uint index = obj.object_id - 1;
            if (index < curExp.carConfigurations.size()) {
                publishDouble(maxVelPubs[index], curExp.carConfigurations[index].velocity);
                publishDouble(maxAccelPubs[index], 0.2 * 9.81);
                publishBool(resetVelPubs[index], true);
                publishBool(clearCommQueuePub,true);
                publishInitTrajectory(trajectoryPubs[index], obj.object_id);

                obj.motion_state.pose.pose.position = curExp.carConfigurations[index].startPos;
                obj.header.stamp = ros::Time::now();
                resetObjectPosePub.publish(obj);
            } else {
                ROS_ERROR("No CarConfig found for vehicle with id %u", obj.object_id);
            }
        }
        ROS_INFO_STREAM("Loaded experiment config " << curExpIndex);
    }

    void Experiment_manager::publishResult() {
        ROS_INFO("Result for experiment %u,%u: time=%f, crash=%s",
                 lastEnvIndex,
                 lastExpIndex,
                 curRes.timeToFinish,
                 curRes.crash ? "true" : "false");
        std::string resultString("" + std::to_string(lastEnvIndex) + ";"
                                 + std::to_string(lastExpIndex) + ";"
                                 + std::to_string(curRes.timeToFinish) + ";"
                                 + (curRes.crash ? "true" : "false"));

        publishString(resultPub, resultString);
        resultToFile(resultString);

    }

    void Experiment_manager::resetResult() {//reset result
        curRes.timeToFinish = 0;
        curRes.crash = false;
    }

    void Experiment_manager::nextIndices() {
        lastEnvIndex = curEnvIndex;
        lastExpIndex = curExpIndex;
        curExpIndex++;

        if (curExpIndex == expConfigs.size()) {
            curExpIndex = 0;
            curEnvIndex++;
            if (curEnvIndex == envConfigs.size()) {
                finished = true;
            }
        }

    }

    void Experiment_manager::publishEnvironmentConfig(EnvConfig &envConfig) {
        setReconfigureParam("time_delay", envConfig.delay);
        setReconfigureParam("drop_probability", envConfig.dropChance);
        publishDouble(timeCutoffPub, envConfig.timeCutoff);
        publishBool(controllerActivePub, envConfig.interConActive);
        for(auto htPub : holdTimePubs) {
            publishDouble(htPub,envConfig.holdTime);
        }
        ROS_INFO_STREAM("Loaded environment config " << curEnvIndex << "=" << envConfig);
    }

    /*
     * deserialization
     */

    void Experiment_manager::loadEnvironmentConfigs() {
        std::ifstream configFile;
        configFile.open(params_.environment_configs_file);
        int lineNumber = 1;
        while (configFile.good()) {
            std::string line;
            getline(configFile, line);
            //skip empty lines
            if(line.size() > 0) {
                std::stringstream stringstream(line);
                EnvConfig tmpEnvConfig;
                try {
                    tmpEnvConfig.delay = readNextAsDouble(stringstream);
                    tmpEnvConfig.dropChance = readNextAsDouble(stringstream);
                    tmpEnvConfig.interConActive = readNextAsBool(stringstream);
                    tmpEnvConfig.timeCutoff = readNextAsDouble(stringstream);
                    tmpEnvConfig.holdTime = readNextAsDouble(stringstream);
                    envConfigs.push_back(tmpEnvConfig);
                    ROS_INFO_STREAM("Loaded EnvConfig=" << tmpEnvConfig);
                } catch (const std::runtime_error &e) {
                    ROS_ERROR("Error parsing the env config file on line %i: %s!", lineNumber, e.what());
                    abort();
                }
            }
            lineNumber++;
        }
        ROS_INFO("Loaded %lu environment configurations", envConfigs.size());
    }

    void Experiment_manager::loadExperimentConfigs() {
        std::map<int, ExperimentConfig> expIdToExpConfig;

        std::ifstream configFile;
        configFile.open(params_.experiments_configs_file);
        int lineNumber = 1;
        while (configFile.good()) {
            std::string line;
            getline(configFile, line);
            if(line.size() > 0) {
                std::stringstream stringstream(line);
                geometry_msgs::Point tmpPoint;
                tmpPoint.z = 0;
                try {
                    int experiment_id = readNextAsInt(stringstream);
                    tmpPoint.x = readNextAsDouble(stringstream);
                    tmpPoint.y = readNextAsDouble(stringstream);
                    double velocity = readNextAsDouble(stringstream);

                    CarConfig tmpCarConfig(tmpPoint, velocity);
                    if (expIdToExpConfig.count(experiment_id) == 0) {
                        expIdToExpConfig[experiment_id] = ExperimentConfig(experiment_id);
                    }
                    expIdToExpConfig[experiment_id].carConfigurations.push_back(tmpCarConfig);
                    ROS_INFO("ExpConfig loaded = (%d,%f,%f,%f)", experiment_id, tmpCarConfig.startPos.x,
                             tmpCarConfig.startPos.y, tmpCarConfig.velocity);
                } catch (const std::runtime_error &e) {
                    ROS_ERROR("Error parsing the exp config file on line %i: %s!", lineNumber, e.what());
                    abort();
                }
            }
            lineNumber++;
        }

        //Add all loaded configs into expConfigs
        for (auto &item : expIdToExpConfig) {
            expConfigs.push_back(item.second);
        }

        ROS_INFO("Loaded %lu experiment configurations", expConfigs.size());
    }

    double Experiment_manager::readNextAsDouble(std::stringstream &stream) {
        std::string tmpString;
        getline(stream, tmpString, ';');
        if (!tmpString.empty()) {
            std::string::size_type curSize;
            double tmpDouble = std::stod(tmpString, &curSize);
            if (curSize == tmpString.size()) {
                return tmpDouble;
            }
        }
        throw std::runtime_error("Error parsing double on \"" + tmpString + "\"");
    }

    int Experiment_manager::readNextAsInt(std::stringstream &stream) {
        std::string tmpString;
        getline(stream, tmpString, ';');
        if (!tmpString.empty()) {
            std::string::size_type curSize;
            int tmpInt = std::stoi(tmpString, &curSize);
            if (curSize == tmpString.size()) {
                return tmpInt;
            }
        }
        throw std::runtime_error("Error parsing int on \"" + tmpString + "\"");
    }

    bool Experiment_manager::readNextAsBool(std::stringstream &stream) {
        std::string tmpString;
        getline(stream, tmpString, ';');
        if (!tmpString.empty()) {
            if (tmpString == "true" || tmpString == "false") {
                bool tmpBool = tmpString == "true";
                return tmpBool;
            }
        }
        throw std::runtime_error("Error parsing bool on \"" + tmpString + "\"");
    }

    void Experiment_manager::resultToFile(std::string &resultString) {
        std::fstream filestream;
        filestream.open(params_.results_file , std::fstream::app);
        filestream << resultString << "\n";
    }

    void Experiment_manager::publishInitTrajectory(ros::Publisher &publisher, unsigned int id) {
        //All trajectoryIn of intersectionController will be parallel
        std_msgs::Float64MultiArray msg;
        msg.data.resize(3*10);
        for(uint i = 0; i < 10; i++){
            //x values
            msg.data[0*10 + i] = 0;
            //y values
            msg.data[1*10 + i] = id*10;
            //dt values
            msg.data[2*10 + i] = i;
        }
        publisher.publish(msg);
    }


}
