<!-- (c) https://github.com/MontiCore/monticore -->
# experiment_manager
Part of the cooperative Intersection scenario(ROS Simulation Framework + EMAM model).

Starts experiments in the simulator and records the results.

## Parameters
### Topics
* **ground\_truth\_topic**: contains current position, velocity, ... of the vehicles
* **collision\_topic**: broadcasts detected collisions between vehicles
* **reset\_object\_pose\_topic**: used to reset the position and rotation of vehicles according to the current experiment configuration
* **controller\_active\_topic**: used to enable/disable the intersection controller
* **clear\_queue\_topic**: used to clear the communication quality queues needed for network delay
* **cutoff\_time\_topic**: cutoff time parameter of the intersection controller. Time slot in which a 2d intersection of trajectories is considered a potential crash

### Topic templates
The topics for each vehicle are calcualted by replacing {i} with the vehicle_id.

* **max\_vel\_topic\_template**: maximum velocity of the vehicles
* **max\_accel\_topic\_template**: maximum acceleration of the vehicles
* **reset\_vel\_topic\_template**: used to reset the velocity of the vehicles
* **hold\_time\_topic\_template**: vehicles keep the same velocity for *hold time* seconds before accelerating
* **trajectory\_topic\_template**: desired trajectory of the vehicles in EMAM format 

### File names
* **experiments\_configs\_file**: Csv file with the experiment configurations, which specify the starting position and velocity of the vehicles. Format: experimentId;x;y;velocity
* **environment\_configs\_file**: Csv file with the environment configurations, which specify the communication quality(packet loss, delay) and the parameters *hold time* as well as *cutoff time*. All experiment configurations are repeated for each environment configuration. Format: delay;dropChance;intersectionControllerActive;timeCutoff;holdTime
* **results\_file**: Csv file for the results. All results are appended. Format: EnvironmentIndex;ExperimentIndex;timeToFinish;crashDetected
